public class Doctorate extends Student{

    private String firstName;
    private String lastName;

    public Doctorate(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        String line = String.format("Dr.%s %s gaus %d € stipendiją", getFirstName(), getLastName(), getScholarShip());
        return line;
    }
    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public int getScholarShip() {
        return 800;
    }
}
