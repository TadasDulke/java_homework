import java.util.Arrays;
import java.util.List;

public class Master extends Student{
    String[] exactSciences = new String[] {"math","physics","biology","chemistry"};
    private String firstName;
    private String lastName;
    private String science;
    private double average;

    public Master(String firstName, String lastName, String science, double average) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.science = science;
        this.average = average;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public int getScholarShip() {
        List<String> sciences = Arrays.asList(exactSciences);
        if(sciences.contains(science.toLowerCase())) {
            return 200;
        }
        return 0;
    }
}
