import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        StudentFactory sf = new StudentFactory();
        String line;
        while ((line = reader.readLine()).length() > 0) {
            try {
                String[] values = line.split(" ");
                String firstName = values[0];
                String lastName = values[1];
                String degree = values[2];
                String science = values[3];
                double average = Double.parseDouble(values[4]);
                Student stud = sf.getStudent(firstName, lastName, degree, science, average);
                System.out.println(stud.toString());
            }catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Bad data formatting. Try again.");
            }
        }
    }
}
