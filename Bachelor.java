import java.util.Arrays;
import java.util.List;

public class Bachelor extends Student{

    private String firstName;
    private String lastName;
    private double average;

    public int getScholarShip() {
        if(average > 8) {
            return 100;
        }
        return 0;
    }

    public Bachelor(String firstName, String lastName, double average) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.average = average;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }
}
