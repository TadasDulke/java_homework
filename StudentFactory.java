public class StudentFactory {

    public Student getStudent(String firstName, String lastName, String degree, String science, double average) {
        if(degree == null) {
            return null;
        }
        if(degree.equalsIgnoreCase("BAKALAURANTAS")) {
            return new Bachelor(firstName, lastName, average);
        }
        else if(degree.equalsIgnoreCase("MAGISTRANTAS")) {
            return new Master(firstName, lastName, science, average);
        }
        else if(degree.equalsIgnoreCase("DOKTORANTAS")) {
            return new Doctorate(firstName, lastName);
        }

        return null;
    }

}
