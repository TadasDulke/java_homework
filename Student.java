public abstract class Student {

    public abstract String getFirstName();
    public abstract String getLastName();
    public abstract int getScholarShip();

    @Override
    public String toString() {
        String line = String.format("%s %s gaus %d € stipendiją", getFirstName(), getLastName(), getScholarShip());
        return line;
    }
}
